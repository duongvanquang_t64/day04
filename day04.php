<?php
$gioitinh = array(0 => "Nam", 1 => "Nữ");

$phankhoa = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

$gioitinhErr = "";
$ngaysinhErr = "";
$phankhoaErr = "";
$nameErr = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['name'])) {
        $nameErr = "Hãy nhập tên.";
    }
    if (empty($_POST['ngaysinh'])) {
        $ngaysinhErr = "Hãy nhập ngày sinh.";
    }
    if (empty($_POST['phankhoa'])) {
        $phankhoaErr = "Hãy chọn phân khoa.";
    }
    if (empty($_POST['gioitinh'])) {
        $gioitinhErr = "Hãy chọn giới tính.";
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        .room {
            width: 700px;
            height: 500px;
            display: block;
            border: 1px solid #007fff;
            margin: auto;
            margin-top: 50px;


        }

        .items {
            display: flex;
            align-items: center;
            margin-left: 60px;
        }

        i {
            color: red;
        }

        .content {
            width: 90px;
            height: 20px;
            padding: 10px 0px;
            text-align: center;
            display: block;
            background-color: #00bf00;
            border: 1px solid #007fff;
        }

        .iptext {
            width: 300px;
            height: 35px;
            border: 1px solid #007fff;
            margin-left: 20px;
        }

        .ipdate {
            height: 35px;
            border: 1px solid #007fff;
            width: 200px;
            margin-left: 20px;
        }

        .phankhoa {
            height: 35px;
            border: 1px solid #007fff;
            margin-left: 20px;
            width: 200px;
        }

        .ipgioitinh {
            margin-left: 20px;
        }

        .btn {
            padding: 15px;
            background-color: #00bf00;
            border: 1px solid #007fff;
            border-radius: 10px;
            margin-left: 250px;

        }

        .post {
            color: red;
            margin-left: 10px;
            margin-top: 20px;
        }
    </style>
</head>

<body>
    <form action="day04.php" method="post">

        <div class="room">
            <span class="post"> <?php echo $nameErr ?></span>
            <span class="post"> <?php echo $gioitinhErr ?></span>
            <span class="post"> <?php echo $phankhoaErr ?></span>
            <span class="post"> <?php echo $ngaysinhErr ?></span>
            <div class="items" style="margin-top: 40px;">
                <p class="content">Họ và Tên<i>*</i></p>
                <input type="text" class="iptext" class="name" name="name">

            </div>
            <div class="items">
                <p class="content" style="margin-right: 20px;">Giới tính<i>*</i></p>


                <?php foreach ($gioitinh as $key => $value) : ?>
                    <label for=<?= $key ?>>
                        <?= $value; ?>
                    </label>
                    <input id=<?= $key ?> require type="radio" name="gioitinh" class="gioitinh" value=<?= $key ?>>
                <?php endforeach; ?>


            </div>
            <div class="items">
                <p class="content">
                    <label for="phankhoa">
                        Phân khoa
                    </label>
                    <i>*</i>
                </p>
                <select name="phankhoa" id="phankhoa" class="phankhoa">
                    <option disabled selected value></option>
                    <?php foreach ($phankhoa as $key => $value) : ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                    <?php endforeach; ?>
                </select>

            </div>
            <div class="items">
                <p class="content">Ngày sinh<i>*</i></p>
                <input type="date" class="ipdate" name="ngaysinh">

            </div>
            <div class="items">
                <p class="content">Địa chỉ</p>
                <input type="text" class="iptext">
            </div>
            <div>
                <input type="submit" value="Đăng Kí" class="btn" name="submit">
            </div>

        </div>
    </form>
</body>

</html>